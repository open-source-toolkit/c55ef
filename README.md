# SimHei.ttf 字体文件下载

## 简介

本仓库提供了一个名为 `SimHei.ttf` 的字体文件下载。该字体文件主要用于解决在 Ubuntu 系统上使用 Python 的 Matplotlib 库时遇到的字体缺失问题。

## 问题描述

在使用 Matplotlib 绘图时，如果系统中缺少 `SimHei` 字体，可能会出现以下错误信息：

```
findfont: Font family [SimHei] not found. Falling back to DejaVu Sans.
```

该错误提示表明 Matplotlib 无法找到 `SimHei` 字体，因此会自动回退到其他默认字体（如 `DejaVu Sans`），这可能会导致图表中的中文显示不正常。

## 解决方案

通过下载并安装 `SimHei.ttf` 字体文件，可以解决上述问题，确保 Matplotlib 能够正确显示中文。

## 使用方法

1. **下载字体文件**：
   - 点击本仓库中的 `SimHei.ttf` 文件进行下载。

2. **安装字体文件**：
   - 将下载的 `SimHei.ttf` 文件复制到系统的字体目录中。通常情况下，Ubuntu 系统的字体目录为 `/usr/share/fonts/truetype/`。
   - 或者，你也可以将字体文件放置在用户目录下的 `.fonts` 文件夹中（例如 `~/.fonts/`）。

3. **更新字体缓存**：
   - 在终端中运行以下命令，更新系统的字体缓存：
     ```bash
     sudo fc-cache -fv
     ```

4. **配置 Matplotlib**：
   - 如果你希望 Matplotlib 默认使用 `SimHei` 字体，可以在代码中进行如下配置：
     ```python
     import matplotlib.pyplot as plt
     plt.rcParams['font.sans-serif'] = ['SimHei']
     plt.rcParams['axes.unicode_minus'] = False
     ```

## 注意事项

- 确保在安装字体文件后，重新启动你的 Python 环境或重新加载 Matplotlib，以使更改生效。
- 如果你在其他操作系统上使用 Matplotlib，可能需要将字体文件放置在相应的字体目录中，并更新字体缓存。

## 贡献

如果你有任何改进建议或发现了其他相关问题，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库中的字体文件遵循其原始许可证。请在使用前确认相关许可条款。